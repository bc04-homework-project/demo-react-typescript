import React from 'react';
import logo from './logo.svg';
import './App.css';
import DemoProps from './DemoProps/DemoProps';

function App() {
  return (
    <div className="App">
      <DemoProps/>
    </div>
  );
}

export default App;
